<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Limelight">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz">
<style>
* {
	box-sizing: border-box;
}

body {
	margin: 0;
	align-items: center;
}

#myVideo {
	position: fixed;
	right: 0;
	bottom: 0;
	min-width: 100%;
	min-height: 100%;
	opacity: 1;
}

.content {
	position: absolute;
	left: 40%;
	top: 12%;
	text-align: center;
	background: rgba(0, 0, 0, 0.7);
	width: 350px;
	height: 700px;
	border-radius: 2%;
}

h1, h2 {
	font-family: Yanone Kaffeesatz;
}

h1, h2, p {
	color: floralwhite;
	font-weight: bold;
}

p {
	font-family: Yanone Kaffeesatz;
	font-size: 18px;
}

.submit {
	font-family: Yanone Kaffeesatz;
	width: 220px;
	font-size: 20px;
	padding: 10px;
	border: 5px black;
	background: floralwhite;
	color: black;
	cursor: pointer;
	word-spacing: 3px;
}

.submit:hover {
	background: activeborder;
	color: white;
}

.input {
	color: black;
	font-weight: bold;
	font-family: Yanone Kaffeesatz;
	font-size: 20px;
	background: floralwhite;
}
</style>


</head>
<body>
	<div>
		<video autoplay muted loop id="myVideo">
			<source src="resources/images/TheArtistShortMP4.mp4" type="video/mp4">
			Your browser does not support HTML5 video.
		</video>

		<div class="content">
			<h1>New Account</h1>
			<form:form action="/register" modelAttribute="userForm">

				<p>Name:</p>
				<form:input type="text" path="name" class="input"
					required="required" />
				<br>
				
				<p>Surname:</p>
				<form:input type="text" path="surname" class="input"
					required="required" />
				<br>

				<p>Email:</p>
				<form:input type="text" path="email" class="input"
					required="required" />
				<br>

				<p>Password:</p>
				<form:input type="password" path="password" class="input"
					required="required" />
				<br>

				<p>Confirm Password:</p>
				<form:input type="password" path="password1" class="input"
					required="required" />
				<br>
				<c:if test="${not empty error}">
   						<p style="color:red">${error}</p>
				</c:if>
				<br>
				<br>
				<input type="submit" class="submit" value="Send" />
				<br>
				<br>

			</form:form>
			<form action="/">
				<input type="submit" class="submit" value="Cancel" /><br>
			</form>

		</div>
	</div>

</body>
</html>