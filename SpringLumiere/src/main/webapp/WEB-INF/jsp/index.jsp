<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>    
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>

    <head>
        <title>Lumière</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="resources/assets/css/main.css" />
        <jsp:include page="inc/netflix.jsp"></jsp:include>
    </head>

	

    <body>
        <!-- Header -->
        <header id="header" class="alt">
            <div class="logo"><a href="#"><img src="resources/images/lumiere.png" style="width:110px;height:70px;"/></a></div>
            <div class="navBar"><a href="/series">Series</a></div>
            <div class="navBar"><a href="/films">Films</a></div>
	        <div class="navBar">
                <input id="searchInput" type="text" style="width: 400px;height: 40px; color: aqua; font-weight: bold;" placeholder="Movies, series, directors, ..."/>
            </div>
            <div class="navBar">
                <button style="width: 100px; height: 40px" class="button" onclick="auxSearch()">Search</button>
            </div>
            
            <a href="#menu">Menu</a>
        </header>
        
        
       
        <!-- Nav -->
        <nav class="navBar" id="menu">
            <ul class="links">
                <li><a href="/myProfile">My profile</a></li>
                <li><a href="/logOut">Log out</a></li>
            </ul>
        </nav>

        
        <!-- Banner -->
        <section class="banner half">
            <article>
                <img src="resources/images/got.jpg" alt="" />
                <div class="inner">
                    <header>
                        <p>Coming soon</p>
                        <h2>Game of Thrones</h2>
                    </header>
                </div>
            </article>
            <article>
                <img src="resources/images/black.jpg" alt="" />
                <div class="inner">
                    <header>
                        <p>Coming soon</p>
                        <h2>Black Panther</h2>
                    </header>
                </div>
            </article>
            <article>
                <img src="resources/images/atomic.jpg"  alt="" />
                <div class="inner">
                    <header>
                        <p>Available now</p>
                        <h2>Atomic Blonde</h2>
                    </header>
                </div>
            </article>
            <article>
                <img src="resources/images/gorrion.jpg"  alt="" />
                <div class="inner">
                    <header>
                        <p>Coming soon</p>
                        <h2>Red Sparrow</h2>
                    </header>
                </div>
            </article>
            <article>
                <img src="resources/images/orangeisthenewblack.jpg"  alt="" />
                <div class="inner">
                    <header>
                        <p>New episodes on June</p>
                        <h2>Orange Is the New Black</h2>
                    </header>
                </div>
            </article>
        </section>
		
		
        
        <section class="content" style="background-color: #071418">
            <!-- " Episodes You are watching" container -->
            <div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Episodes you are watching</b></h2>
                </header>
                <div class="rowN">
                    <div class="rowN__inner">
                    		<c:forEach var="episodes" items="${episodesNotFinished}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${episodes.getEpisode().getSeason().getSeries().getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/serieView/${episodes.getEpisode().getSeason().getSeries().idSeries}"><div class="tile__title">${episodes.getEpisode().getSeason().getSeries().title}: ${episodes.getEpisode().getTitle()}</div></a>
	                            </div>
	                            <div id="myProgress">
	                                <div id="myBar" style="width: ${episodes.barTimeCalculation()}%"></div>
	                            </div>
	                        </div>
						</c:forEach>
                    </div>
                </div>
            </div>
			
			<!-- "Films you are watching" container -->
			<div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Films you are watching</b></h2>
                </header>
                <div class="rowN">
                    <div class="rowN__inner">
                    		<c:forEach var="film" items="${filmsNotFinished}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${film.getFilm().getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/filmView/${film.getFilm().idFilm}"><div class="tile__title">${film.getFilm().getTitle()}</div></a>
	                            </div>
	                            <div id="myProgress">
	                                <div id="myBar" style="width: ${film.barTimeCalculation()}%"></div>
	                            </div>
	                        </div>
	                        <div id="filmViewed" style="display: none;">${film.time}</div>
	                        
						</c:forEach>
                    </div>
                </div>
            </div>
            
            <!-- "latest series" container -->
            <div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Latest series</b></h2>
                </header>
                <div class="rowN">
               		<div class="rowN__inner">
	                    <c:forEach var="latest" items="${latestSeries}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${latest.getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/serieView/${latest.idSeries}"><div class="tile__title">${latest.getTitle()}</div></a>
	                            </div>
	                        </div>
						</c:forEach>
                    </div>
                </div>
            </div>
            
			
            <!-- "latest films" container -->
            <div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Latest films</b></h2>
                </header>
                <div class="rowN">
               		<div class="rowN__inner">
	                    <c:forEach var="latest" items="${latestFilms}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${latest.getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/filmView/${latest.getIdFilm()}"><div class="tile__title">${latest.getTitle()}</div></a>
	                            </div>
	                        </div>
						</c:forEach>
                    </div>
                </div>
            </div>

			<!-- "Top Rated (By Lumiere users)" container -->
             <div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Top Rated Series</b></h2>
                    <h3>Rated by Lumière users</h3>
                </header>
                <div class="rowN">
               		<div class="rowN__inner">
	                    <c:forEach var="seriesLumiere" items="${topRatedSeriesLumiereUsers}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${seriesLumiere.getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/serieView/${seriesLumiere.idSeries}"><div class="tile__title">${seriesLumiere.getTitle()}</div></a> 
	                            </div>
	                        </div>
						</c:forEach>
                    </div>
                </div>
            </div>
            
            <!-- "Top Rated (By Filmaffinity users)" container -->
             <div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Top Rated Series</b></h2>
                    <h3>Rated by IMDb users</h3>
                </header>
                <div class="rowN">
               		<div class="rowN__inner">
	                    <c:forEach var="seriesFilmaffinity" items="${topRatedSeriesFilmaffinityUsers}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${seriesFilmaffinity.getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/serieView/${seriesFilmaffinity.idSeries}"><div class="tile__title">${seriesFilmaffinity.getTitle()}</div></a>
	                            </div>
	                        </div>
						</c:forEach>
                    </div>
                </div>
            </div>
			
            <!-- "Top Rated (By Lumiere users)" container -->
             <div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Top Rated Films</b></h2>
                    <h3>Rated by Lumière users</h3>
                </header>
                <div class="rowN">
               		<div class="rowN__inner">
	                    <c:forEach var="filmsLumiere" items="${topRatedFilmsLumiereUsers}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${filmsLumiere.getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/filmView/${filmsLumiere.idFilm}"><div class="tile__title">${filmsLumiere.getTitle()}</div></a>
	                            </div>
	                        </div>
						</c:forEach>
                    </div>
                </div>
            </div>
            
            <!-- "Top Rated (By Filmaffinity users)" container -->
             <div class="container">
                <header>
                    <h2><b style="color: #FAFAFA">Top Rated Films</b></h2>
                    <h3>Rated by IMDb users</h3>
                </header>
                <div class="rowN">
               		<div class="rowN__inner">
	                    <c:forEach var="filmsFilmaffinity" items="${topRatedFilmsFilmaffinityUsers}">
  					     	<div class="tile">
	                            <div class="tile__media">
	                                <img class="tile__img" src="${filmsFilmaffinity.getImageNetflix()}"/>
	                            </div>
	                            <div class="tile__details">
	                                <a href="/filmView/${filmsFilmaffinity.idFilm}"><div class="tile__title">${filmsFilmaffinity.getTitle()}</div></a>
	                            </div>
	                        </div>
						</c:forEach>
                    </div>
                </div>
            </div>
        </section>
        
        <div id="filmDuration" style="display: none;">${film.getFilm().duration}</div>
        
        <!-- Footer -->
        <footer id="footer">
            <div class="container" style="display: none">
                <ul class="icons">
                    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                </ul>
            </div>
            <div class="copyright">
                &copy; Lumière. All rights reserved.
            </div>
        </footer>

        <!-- Scripts -->
        <script src="resources/assets/js/jquery.min.js"></script>
        <script src="resources/assets/js/jquery.scrollex.min.js"></script>
        <script src="resources/assets/js/skel.min.js"></script>
        <script src="resources/assets/js/util.js"></script>
        <script src="resources/assets/js/main.js"></script>		
        <script src="resources/assets/js/searchJS.js"></script>		
        
        <div id="hiddenSearch" style="display: none;">
        		<form:form id="formSearch" method="GET">
        			<button id="buttonSearch" style="width: 100px; height: 40px" class="button" >Search</button>
        		</form:form>
        </div>
    </body>
</html>
