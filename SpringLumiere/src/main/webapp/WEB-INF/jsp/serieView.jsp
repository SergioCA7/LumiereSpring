<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>
<html>
<head>
<title>Lumière</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="../resources/assets/css/main.css" />
</head>
<body>

	<!-- Header -->
	<header id="header" class="alt">
		<div class="logo">
			<a href="/index"><img src="../resources/images/lumiere.png"
				style="width: 110px; height: 70px;" /></a>
		</div>
		<div class="navBar">
			<a href="/series">Series</a>
		</div>
		<div class="navBar">
			<a href="/films">Films</a>
		</div>
		<div class="navBar">
			<input id="searchInput" type="text" style="width: 400px; height: 40px; color: aqua; font-weight: bold;"
				placeholder="Movies, series, directors, ..." />
		</div>
		<div class="navBar">
			<button style="width: 100px; height: 40px" class="button" onclick="auxSearch()">Search</button>
		</div>
		<a href="#menu">Menu</a>
	</header>


	<!-- Nav -->
	<nav class="navBar" id="menu">
		<ul class="links">
			<li><a href="/myProfile">My profile</a></li>
			<li><a href="/logOut">Log out</a></li>
		</ul>
	</nav>


	<!-- Serie title -->
	<section id="two" class="wrapper style3">
		<div class="inner">
			<header class="align-center">
				<br>
				<h2>
					<div id="titleSerie"> <b>${serie.title}</b> </div>
				</h2>
			</header>
		</div>
	</section>


	<!-- Serie quote -->
	<section id="three" class="wrapper style2">
		<div class="inner">
			<header class="align-center">
				<p class="special" style="color: #0A2A29">"${serie.quote}"</p>
				<h1>${serie.quoteAuthor}</h1>
			</header>
		</div>
	</section>


	<!-- Serie details -->
	<section id="one" class="wrapper style2">
		<div class="inner">
			<div class="grid-style">
				<!-- Poster container -->
				<div>
					<div class="box" style="background-color: #0A2A29">
						<div class="image fit">
							<img src="${serie.imagePoster}" alt="" />
						</div>

						<div class="content">
							<header class="align-center">
								<p>
									<b>Popularity</b>
								</p>
							</header>
							<p style="text-align: center">${serie.ourRating}</p>

						</div>
					</div>
				</div>
				<!-- Details container -->
				<div>
					<div class="box" style="background-color: #0A2A29">
						<div class="content">
							<header class="align-center">
								<p>
									<b>Sinopsis</b>
								</p>
							</header>
							<p style="text-align: justify">
								(${serie.origen}, ${serie.year}) <br>${serie.sinopsis}</p>
							<header class="align-center">
								<p>
									<b>Genre</b>
								</p>
							</header>
							<p style="text-align: center">${serie.genders}</p>
							<header class="align-center">
								<p>
									<b>Seasons</b>
								</p>
							</header>
							<div style="height: 600px; overflow: auto">
								<!-- Season 1 -->

								<table>
									<table>
										<c:forEach items="${serie.seasonCollection}" var="season">
											<tr>
												<td><b style="color: white">Season
														${season.numberSeason}</b></td>

												<c:forEach items="${season.episodeCollection}" var="episode">
													<tr>
														<td><a id="playVideo" onclick="episodePetition(' ${episode.title.toString()} ')" href="#img1" style="color: white">${episode.title}</a></td>
													</tr>
												</c:forEach>
										</c:forEach>
									</table>
								</table>
								<a href="#" class="lightbox" id="img1" onclick="closeVideo()" onclose="closeVideo()">
									<div id="videoModal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel"
										aria-hidden="false" style="display: block;" >
										<div class="modal-header">
											<button type="button" class="close full-height"
												data-dismiss="modal" aria-hidden="true" onclick="closeVideo()">X</button>
											<h3 style="color: white;" id="titleVideo"></h3>
										</div>
										<div class="modal-body">
											<video id="videoStreaming" width="870" height="489" controls>
												<source src="">
											</video>
										</div>
										<div class="modal-footer"></div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>







	<!-- Footer -->
	<footer id="footer">
		<div class="container" style="display: none">
			<ul class="icons">
				<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
				<li><a href="#" class="icon fa-facebook"><span
						class="label">Facebook</span></a></li>
				<li><a href="#" class="icon fa-instagram"><span
						class="label">Instagram</span></a></li>
				<li><a href="#" class="icon fa-envelope-o"><span
						class="label">Email</span></a></li>
			</ul>
		</div>
		<div class="align-center">
		<script type='text/javascript'>
		var title = $("#titleSerie").text();
		var titleFilm = "'" + title + "'";
		var searchTitle = "'" + title + " available films'";

		var amzn_wdgt = {
			widget : 'Carousel'
		};
		amzn_wdgt.tag = 'amawid-21';
		amzn_wdgt.widgetType = 'SearchAndAdd';
		amzn_wdgt.searchIndex = 'DVD';
		amzn_wdgt.title = searchTitle;
		amzn_wdgt.width = '600';
		amzn_wdgt.keywords = titleFilm;
		amzn_wdgt.height = '200';
		amzn_wdgt.marketPlace = 'GB';
		amzn_wdgt.shuffleProducts = 'True';
		</script>
		<script type='text/javascript'
			src='http://wms-eu.amazon-adsystem.com/20070822/GB/js/swfobject_1_5.js'>
			
		</script>
	</div>
		<div class="copyright">&copy; Lumière. All rights reserved.</div>
	</footer>
		

	<!-- Scripts -->
	<script src="../resources/assets/js/jquery.min.js"></script>
	<script src="../resources/assets/js/jquery.scrollex.min.js"></script>
	<script src="../resources/assets/js/skel.min.js"></script>
	<script src="../resources/assets/js/util.js"></script>
	<script src="../resources/assets/js/main.js"></script>
	<script src="../resources/assets/js/episodeStreaming.js"></script>
	<script src="../resources/assets/js/searchJS.js"></script>		
        
        <div id="hiddenSearch" style="display: none;">
        		<form:form id="formSearch" method="GET">
        			<button id="buttonSearch" style="width: 100px; height: 40px" class="button" >Search</button>
        		</form:form>
        </div>
	

</body>
</html>