<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Limelight">
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>
        <style>

            * {
                box-sizing: border-box;
            }

            body {
                margin: 0;

            }

            #myVideo {
                position: fixed;
                right: 0;
                bottom: 0;
                min-width: 100%; 
                min-height: 100%;
                opacity: 1;
            }

            .content {
                position: absolute;
                left: 0;
                top: 30%;
                width: 100%;
                text-align: center;

            }

            h1{
                display: inline-block;
                position: relative;
                font-family:   Limelight; 
                text-shadow: 1px 2px 30px white;
                font-size: 45px;
                /*   border-bottom: 3px solid antiquewhite;
                   border-bottom-left-radius: 50%;
                   border-bottom-right-radius: 50%;
                   border-width: 3px;
                   border-spacing: 10px;
                   bottom: 50%;*/
            }
            h1::after {
                content: '';
                position: absolute;
                left: 0;
                display: inline-block;
                height: 1.2em;
                width: 100%;
                border-bottom: 3px solid;

                border-bottom-left-radius: 10%;
                border-bottom-right-radius: 10%;

                margin-top: 5px;
            }
            h1,p  {
                color: floralwhite;
                font-weight:bold;
            }
            p{
                font-family:   Yanone Kaffeesatz; 
                font-size: 20px;

            }

            button {

                font-family:   Yanone Kaffeesatz; 
                width: 220px;
                font-size: 20px;
                padding: 10px;
                border: 5px black;
                background: floralwhite;
                color: black;
                cursor: pointer;
                word-spacing: 3px;


            }


            button:hover {
                background: lightskyblue;
                color: white;
            }

        </style>
    </head>
    
    
    <body>
        <div>
            <video autoplay muted loop id="myVideo"> 
                <source src="../resources/images/TheArtistShortMP4.mp4" type="video/mp4">
                Your browser does not support HTML5 video.
            </video>

            <div class="content">
                <img src="../resources/images/lumiere.png"/>
                <p>Your cinema,<br>and every day of more people</p><br>
                <div style="text-align: center">
                 <form style="display: inline-block;" action = "/goLogin">
                		<button  id="myBtnLoginIn" >Login</button>
                </form>
                <form style="display: inline-block;" method = "GET" action = "/goRegister">
                		<button id="myBtnRegister" >Hire</button>
                	<form>
                	</div>
            </div>
        </div>

       
        
    </body>
</html>