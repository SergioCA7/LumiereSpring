<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>

    <head>
        <title>Lumière</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="resources/assets/css/main.css" />
        <jsp:include page="inc/netflix.jsp"></jsp:include>
    </head>


    <body>
        <!-- Header -->
        <header id="header" class="alt">
            <div class="logo"><a href="/index"><img src="resources/images/lumiere.png" style="width:110px;height:70px;"/></a></div>
            <div class="navBar"><a href="/series">Series</a></div>
            <div class="navBar"><a href="/films">Films</a></div>
            <div class="navBar">
                <input id="searchInput" type="text" style="width: 400px;height: 40px;  color: aqua; font-weight: bold;" placeholder="Movies, series, directors, ..."/>
            </div>
            <div class="navBar">
                <button style="width: 100px;height: 40px" class="button" onclick="auxSearch()">Search</button>
            </div>
            <a href="#menu">Menu</a>
        </header>


        <!-- Nav -->
        <nav class="navBar" id="menu">
            <ul class="links">
                <li><a href="/myProfile">My profile</a></li>
                <li><a href="/logOut">Log out</a></li>
            </ul>
        </nav>


        <!-- Header quote -->
        <section id="two" class="wrapper style3">
            <div class="inner">
                <header class="align-center">
                    <br>
                    <p><b>You can call me Bond... James Bond.</b></p>
                </header>
            </div>
        </section>


        <div style="text-align: center">
            <form:form action="/editPassword" modelAttribute="userForm" style="text-align: center">
                <br><h1><b>Name:</b></h1>
                <h1>${user.name}</h1>
                <h1><b>Surname:</b></h1>
                <h1>${user.surname}</h1>
                <h1><b>Email:</b></h1>
                <h1>${user.email}</h1>
                <h1><b>Password:</b></h1>
                <form:input type="password" path="password" style="width: 300px; margin: 0 auto; background-color: cadetblue; border-color: black"/><br>
                <h1><b>Confirm password:</b></h1>
                <form:input type="password" path="password1" style="width: 300px; margin: 0 auto; background-color: cadetblue; border-color: black"/><br>
                <br>
                <c:if test="${not empty error}">
   					<h1 style="color:red; font-weight: bold;">${error}</h1>
				</c:if>
                <br><input type="submit" class="submit" value="Change" style="background: cadetblue">
            </form:form>
        </div>


        <!-- Footer -->
        <footer id="footer">
            <div class="container" style="display: none">
                <ul class="icons">
                    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                </ul>
            </div>
            <div class="copyright">
                &copy; Lumière. All rights reserved.
            </div>
        </footer>


        <!-- Scripts -->
        <script src="resources/assets/js/jquery.min.js"></script>
        <script src="resources/assets/js/jquery.scrollex.min.js"></script>
        <script src="resources/assets/js/skel.min.js"></script>
        <script src="resources/assets/js/util.js"></script>
        <script src="resources/assets/js/main.js"></script>
        <script src="resources/assets/js/searchJS.js"></script>		
<div id="hiddenSearch" style="display: none;">
        		<form:form id="formSearch" method="GET">
        			<button id="buttonSearch" style="width: 100px; height: 40px" class="button" >Search</button>
        		</form:form>
        </div>
    </body>
</html>
