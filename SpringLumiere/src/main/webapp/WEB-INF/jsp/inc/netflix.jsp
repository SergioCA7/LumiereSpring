<%-- 
    Document   : netflix
    Created on : 08-mar-2018, 18:21:30
    Author     : inftel01
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src='//static.codepen.io/assets/editor/live/console_runner-ce3034e6bde3912cc25f83cccb7caa2b0f976196f2f2d52303a462c826d54a73.js'></script>
<script src='//static.codepen.io/assets/editor/live/css_live_reload_init-890dc39bb89183d4642d58b1ae5376a0193342f9aed88ea04330dc14c8d52f55.js'></script>
<meta charset='UTF-8'>
<meta name="robots" content="noindex">
<link rel="shortcut icon" type="image/x-icon" href="//static.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
<link rel="mask-icon" href="//static.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg"/>
<link rel="canonical" href="https://codepen.io/italoag/pen/jVNLVo" />


<style class="cp-pen-styles">
	bodyN, htmlw {
	  padding: 0 10px;
	  margin: 0;
	  background: #0e0f11;
	  color: #ecf0f1;
	  font-family: 'Open Sans', sans-serif;
	  min-height: 100vh;
	  display: flex;
	  flex-direction: row;
	  align-items: center;
	  width: 100%;
	}

	* {
	  box-sizing: border-box;
	}

	h1 {
	  text-align: center;
	}

	pN {
	  text-align: center;
	  width: 100%;
	  max-width: 500px;
	  margin: auto;
	}

	a:link, a:hover, a:active, a:visited {
	  transition: color 150ms;
	  color: #95a5a6;
	  text-decoration: none;
	}
	a:hover {
	  color: #7f8c8d;
	  text-decoration: underline;
	}

	.containN {
	  width: 100%;
	}

	.rowN {
	  overflow: scroll;
	  width: 100%;
	}

	.rowN__inner {
	  transition: 450ms transform;
	  font-size: 0;
	  white-space: nowrap;
	  margin: 70.3125px 0;
	  padding-bottom: 10px;
	}

	.tile {
	  position: relative;
	  display: inline-block;
	  width: 250px;
	  height: 140.625px;
	  margin-right: 10px;
	  font-size: 20px;
	  cursor: pointer;
	  transition: 450ms all;
	  transform-origin: center left;
	}

	.tile__img {
	  width: 250px;
	  height: 140.625px;
	  object-fit: cover;
	}

	.tile__details {
	  position: absolute;
	  bottom: 0;
	  left: 0;
	  right: 0;
	  top: 0;
	  font-size: 10px;
	  opacity: 0;
	  background: linear-gradient(to top, rgba(0, 0, 0, 0.9) 0%, rgba(0, 0, 0, 0) 100%);
	  transition: 450ms opacity;
	}
	.tile__details:after, .tile__details:before {
	  content: '';
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  display: #000;
	}
	/*.tile__details:after {
	  margin-top: -25px;
	  margin-left: -25px;
	  width: 50px;
	  height: 50px;
	  border: 3px solid #ecf0f1;
	  line-height: 50px;
	  text-align: center;
	  border-radius: 100%;
	  background: rgba(0, 0, 0, 0.5);
	  z-index: 1;
	}*/
	/**.tile__details:before {
	  content: '▶';
	  left: 0;
	  width: 100%;
	  font-size: 35px;
	  margin-left: 7px;
	  margin-top: -19px;
	  text-align: center;
	  z-index: 2;
	}**/

	.tile:hover .tile__details {
	  opacity: 1;
	}

	.tile__title {
	  position: absolute;
	  bottom: 0;
	  right: 0;
	  padding: 10px;
	}

	.rowN__inner:hover {
	  transform: translate3d(-125px, 0, 0);
	}
	.rowN__inner:hover .tile {
	  opacity: 0.3;
	}
	.rowN__inner:hover .tile:hover {
	  transform: scale(2);
	  opacity: 1;
	}

	.tile:hover ~ .tile {
	  transform: translate3d(250px, 0, 0);
	}
</style>
