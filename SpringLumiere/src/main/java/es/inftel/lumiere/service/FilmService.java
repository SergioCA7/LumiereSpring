package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import es.inftel.lumiere.entity.Film;

public interface FilmService {

	Map<String, List<Film>> findFilmByGenresMap();
	List<Film> findTrendyFilms();
	List<Film> findLatestFilms();
	List<Film> findTopRatedFilmsLumiereUsers();
	List<Film> findTopRatedFilmsFilmaffinityUsers();
	List<Film> getFilmsByTitle(String text);
	List<Film> getFilmsByDirector(String text);
	Film findOne(BigDecimal id2);
	Film findFilmByID(BigDecimal id2);

}
