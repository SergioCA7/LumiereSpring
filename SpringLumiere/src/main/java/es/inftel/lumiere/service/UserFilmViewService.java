package es.inftel.lumiere.service;

import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userfilmview;
import es.inftel.lumiere.entity.UserfilmviewPK;

@Service
public interface UserFilmViewService {

	void deleteUserFilmView(UserfilmviewPK ufvPK);
	void createUserFilmView(Userfilmview ufv);
}
