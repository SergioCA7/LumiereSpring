package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import es.inftel.lumiere.entity.Users;




public interface UserService {

	 Users getUser(String email, String password) throws NoSuchAlgorithmException, InvalidKeySpecException;
	 Users checkUserEmail(String email);
	 void save (Users user) throws NoSuchAlgorithmException, InvalidKeySpecException;
	 Users findById(BigDecimal id);
	 void updatePass(String pass, BigDecimal id_user);

}	    
