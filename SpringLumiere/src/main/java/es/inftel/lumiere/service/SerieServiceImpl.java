package es.inftel.lumiere.service;

import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedHashMap;

import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Series;
import es.inftel.lumiere.repository.SerieRepository;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class SerieServiceImpl implements SerieService{

	@Autowired
	SerieRepository serieRepository;

	@Override
	public Map<String, List<Series>> findSerieByGenresMap() {
		Map<String, List<Series>> serieMap = new LinkedHashMap<>();
		List<String> genreList = serieRepository.findGenres();
		for (String s : genreList) {
			serieMap.put(s , serieRepository.findSeriesByGenre(s));
		}
		return serieMap;
	}


	@Override
	public Series findSerieByID(BigDecimal id) {
		return serieRepository.findOne(id);
	}
	
	@Override
	public List<Series> findLatestSeries() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return serieRepository.findLatestSeries(BigDecimal.valueOf(year));
	}
	
	@Override
	public List<Series> findTopRatedSeriesLumiereUsers(){
		return serieRepository.findTopRatedSeriesLumiereUsers();
	}
	
	@Override
	public List<Series> findTopRatedSeriesFilmaffinityUsers(){
		return serieRepository.findTopRatedSeriesFilmaffinityUsers();
	}

	@Override
	public List<Series> getSeriesByTitle(String text) {
		return serieRepository.getSeriesByTitle(text);
	}

	
}
