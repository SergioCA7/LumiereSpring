package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.UserRepository;
import es.inftel.lumiere.utils.Utils;





@Service
public class UserServiceImpl implements UserService {
	 @Autowired
	 UserRepository userRepository;
	

	@Override
	public Users getUser(String email, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		Users user = userRepository.checkUserEmail(email);
		if(user != null) {
			user = Utils.validatePassword(password,user.getPass()) ? user : null;
		}
		return user;	
	}

	@Override
	public Users checkUserEmail(String email) {
		return userRepository.checkUserEmail(email);
	}

	@Override
	public void save(Users user) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String password = Utils.generateStorngPasswordHash(user.getPass());
		user.setPass(password);
		userRepository.save(user);
	}
	

	public Users findById(BigDecimal id) {
		return userRepository.findOne(id);
	}
	
	public void updatePass(String pass, BigDecimal id_user) {
		try {
			userRepository.updatePass(Utils.generateStorngPasswordHash(pass), id_user);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	
}	    
