package es.inftel.lumiere.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.UserepisodenofinishRepository;

@Service
public class UserepisodenofinishImpl implements UserepisodenofinishService{
	
	
	@Autowired 
	UserepisodenofinishRepository userEpisodenofinishRepository;
	
	public List<Userepisodenofinish> findEpisodiesNotFinished (Users user){
		return (List<Userepisodenofinish>) this.userEpisodenofinishRepository.findEpisodiesNotFinished(user);
		
	}
	
	
}
