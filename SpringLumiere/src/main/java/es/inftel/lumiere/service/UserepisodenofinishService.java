package es.inftel.lumiere.service;

import java.util.List;

import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.Users;


public interface UserepisodenofinishService {
	List<Userepisodenofinish> findEpisodiesNotFinished (Users user);

}
