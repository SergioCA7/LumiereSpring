package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.inftel.lumiere.entity.Film;
import es.inftel.lumiere.repository.FilmRepository;

@Service
public class FilmServiceImpl implements FilmService {
	@Autowired
	FilmRepository filmRepository;

	@Override
	public Map<String, List<Film>> findFilmByGenresMap() {
		Map<String, List<Film>> genres = new LinkedHashMap<>();
		List<String> genresList = filmRepository.findGenres();
		for (String g : genresList) {
			genres.put(g, filmRepository.findFilmByGenre(g));
		}

		return genres;

	}
	
	@Override
	public List<Film> findTrendyFilms() {
		return filmRepository.findTrendyFilms();
	}
	
	@Override
	public List <Film> findLatestFilms(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return filmRepository.findLatestFilms(BigDecimal.valueOf(year));
	}
	
	@Override
	public List<Film> findTopRatedFilmsLumiereUsers(){
		return filmRepository.findTopRatedFilmsLumiereUsers();
		
	}
	
	@Override
	public List<Film> findTopRatedFilmsFilmaffinityUsers(){
		return filmRepository.findTopRatedFilmsFilmaffinityUsers();
		
	}

	@Override
	public List<Film> getFilmsByTitle(String text) {
		return filmRepository.getFilmsByTitle(text);
	}	
		
	@Override
	public List<Film> getFilmsByDirector(String text) {	
		return filmRepository.getFilmsByDirector(text);
	}

	@Override
	public Film findOne(BigDecimal id2) {
		// TODO Auto-generated method stub
		return filmRepository.findOne(id2);
	}

	@Override
	public Film findFilmByID(BigDecimal id2) {
		// TODO Auto-generated method stub
		return filmRepository.findOne(id2);
	}

}
