package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import es.inftel.lumiere.entity.Series;


public interface SerieService {
	
	Map<String, List<Series>> findSerieByGenresMap();
	
	Series findSerieByID(BigDecimal id);
	
	List<Series> findLatestSeries();
	List<Series> findTopRatedSeriesLumiereUsers();
	List<Series> findTopRatedSeriesFilmaffinityUsers();
	List<Series> getSeriesByTitle(String text);

}
