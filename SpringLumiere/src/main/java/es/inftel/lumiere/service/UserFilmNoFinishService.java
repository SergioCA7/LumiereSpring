package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.UserfilmnofinishPK;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.UserFilmNoFinishRepository;

@Service
public interface UserFilmNoFinishService {
	
	void save(Userfilmnofinish userNF);
	Userfilmnofinish findOne(UserfilmnofinishPK pk);
	void updateTime(BigDecimal bigDecimal, UserfilmnofinishPK ufnf);
	void deleteFilm(UserfilmnofinishPK ufnf);
	List<Userfilmnofinish> findFilmsNotFinished (Users user);
	boolean exists(UserfilmnofinishPK id);
}
