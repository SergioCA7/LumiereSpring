package es.inftel.lumiere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLumiereApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLumiereApplication.class, args);
	}
}
