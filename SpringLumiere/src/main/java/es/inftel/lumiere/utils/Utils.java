package es.inftel.lumiere.utils;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class Utils {

    private final static String PBKDF_ALGORITHM = "PBKDF2WithHmacSHA256";
    private final static String SECURE_RANDOM_ALGORITHM = "SHA1PRNG";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Generates a hashed password from a String.
     *
     * <p>
     * The password has three parts separated by {@code :}, the first is the
     * number of iterations used on {@code PBEKeySpec}, the second, the hash of
     * the given String, and third, a CSPRN Salt.
     *
     * @param password The String to be hashed
     *
     * @return The hashed String
     *
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static String generateStorngPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(Utils.PBKDF_ALGORITHM);
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    /**
     * Generates a Cryptographically Secure Pseudo-Random Number Salt byte
     * array.
     *
     * @return A 20 bytes array Salt
     *
     * @throws NoSuchAlgorithmException
     */
    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance(Utils.SECURE_RANDOM_ALGORITHM);
        byte[] salt = new byte[20];
        sr.nextBytes(salt);
        return salt;
    }

    /**
     * Converts a byte array to a hex String.
     *
     * @param array The byte array to be converted
     *
     * @return A hex String containing the converted bytes
     *
     * @throws NoSuchAlgorithmException
     */
    private static String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    /**
     * Validates a password by comparing a plain password string by a hashed
     * counterpart.
     *
     * @param originalPassword The plain password string
     * @param storedPassword The hashed password string
     *
     * @return {@code True} if passwords match {@code False} if passwords don't
     * match
     *
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static boolean validatePassword(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(Utils.PBKDF_ALGORITHM);
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    /**
     * Converts a hex String to a byte array.
     *
     * @param hex The hex String to be converted
     *
     * @return An array of bytes representing the hex String
     *
     * @throws NoSuchAlgorithmException
     */
    private static byte[] fromHex(String hex) throws NoSuchAlgorithmException {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    /**
     * Converts a String to a Date.
     *
     * @param date The String date representation to convert
     *
     * @return A Date
     *
     * @throws ParseException
     */
    public static Date stringToDate(String date) throws ParseException {
        return simpleDateFormat.parse(date);
    }

    /**
     * Converts a Date to a String.
     *
     * @param date The date representation to convert
     *
     * @return A String
     *
     */
    public static String dateToString(Date date) {
        return simpleDateFormat.format(date);
    }
}