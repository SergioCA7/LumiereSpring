package es.inftel.lumiere.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import es.inftel.lumiere.controller.FilmController.STResourceHttpRequestHandler;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.FilmRepository;
import es.inftel.lumiere.repository.SecurityRepository;
import es.inftel.lumiere.service.FilmService;
import es.inftel.lumiere.service.SerieService;
import es.inftel.lumiere.service.UserFilmNoFinishService;
import es.inftel.lumiere.service.UserService;
import es.inftel.lumiere.service.UserepisodenofinishService;

@Controller
public class Controlador {

	@Autowired
    private STResourceHttpRequestHandler handler;
	
	@Autowired
	UserService userService;

	@Autowired
	UserepisodenofinishService userepisodenofinishService;
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	UserFilmNoFinishService userfilmnofinishService;
	
	@Autowired
	SerieService seriesService;
   

	
	
    @Autowired
    protected FilmRepository repository;
    
    @Autowired
    protected SecurityRepository securityRepository;
    


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String inicio (Model model) {
		return "home";
	}
	
	@RequestMapping(value = "/goLogin", method = RequestMethod.GET)
	public String goLogin(Model model) {
		model.addAttribute("userLogin", new Users());
		return "login";
	}
	@RequestMapping(value = "/goRegister", method = RequestMethod.GET)
	public String goRegister(Model model) {
		model.addAttribute("userForm", new UserForm());
		
		return "register";
	}
	
	@RequestMapping(value = "/filmView", method = RequestMethod.GET)
	public String goToFilmView(Model modelo) {
	
		return "filmView";
	}
	

	
	@RequestMapping(value = "/serieView", method = RequestMethod.GET)
	public String goToSerieView(Model modelo) {
	
		return "serieView";
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String goIndex(Model model,HttpServletRequest request) {
		String res = "home";
		Users  user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null) {
			
			model.addAttribute("episodesNotFinished",this.userepisodenofinishService.findEpisodiesNotFinished(user));
			model.addAttribute("filmsNotFinished", this.userfilmnofinishService.findFilmsNotFinished(user));
			model.addAttribute("latestSeries", this.seriesService.findLatestSeries());
			model.addAttribute("latestFilms", this.filmService.findLatestFilms());
			model.addAttribute("topRatedSeriesFilmaffinityUsers",this.seriesService.findTopRatedSeriesFilmaffinityUsers());
			model.addAttribute("topRatedSeriesLumiereUsers",this.seriesService.findTopRatedSeriesLumiereUsers());
			model.addAttribute("topRatedFilmsFilmaffinityUsers", this.filmService.findTopRatedFilmsFilmaffinityUsers());
			model.addAttribute("topRatedFilmsLumiereUsers", this.filmService.findTopRatedFilmsLumiereUsers());
			res = "index";	
		}
		return res;
	}
}
