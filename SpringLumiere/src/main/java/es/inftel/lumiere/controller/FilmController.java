package es.inftel.lumiere.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import es.inftel.lumiere.entity.Film;
import es.inftel.lumiere.entity.Series;
import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.UserfilmnofinishPK;
import es.inftel.lumiere.entity.Userfilmview;
import es.inftel.lumiere.entity.UserfilmviewPK;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.FilmRepository;
import es.inftel.lumiere.repository.UserFilmViewRepository;
import es.inftel.lumiere.service.FilmService;
import es.inftel.lumiere.service.SerieService;
import es.inftel.lumiere.service.UserFilmNoFinishService;

@Controller
public class FilmController {
	@Autowired
	FilmService filmService;
	
	@Autowired
    private STResourceHttpRequestHandler handler;
	
	@Autowired
	SerieService serieService;
	
	@Autowired
	protected FilmRepository filmRepository;
	
	@Autowired
	UserFilmNoFinishService userFNFS;
	@Autowired
	UserFilmViewRepository userFV;
	
	
	@CrossOrigin
    @GetMapping("/stream/{title}")
    public void home(HttpServletRequest request, HttpServletResponse response, @PathVariable("title") String title) throws ServletException, IOException {
		if(title.contains("%20")) {
			title.replaceAll("%20", "");
		}
		title = title.replaceAll("\u00A0", "");
		title = title.replaceAll(" ", "");
		title = title.replaceAll(":", "");
		String loc = "/Users/inftel01/Documents/Proyecto Web/LumiereSpring/SpringLumiere/src/main/webapp/resources/images/" + title + ".mp4";
		File MP4_FILE = new File(loc);
		if(!MP4_FILE.exists()){
			//File por defecto
			 loc = "/Users/inftel01/Documents/Proyecto Web/LumiereSpring/SpringLumiere/src/main/webapp/resources/images/crop.mp4";
			 MP4_FILE = new File(loc);
		}
        request.setAttribute(STResourceHttpRequestHandler.ATTR_FILE, MP4_FILE);
        handler.handleRequest(request, response);
    }
		
		@Component
	    final static class STResourceHttpRequestHandler extends ResourceHttpRequestHandler {

	        final static String ATTR_FILE = STResourceHttpRequestHandler.class.getName() + ".file";

	        @Override
	        protected Resource getResource(HttpServletRequest request) throws IOException {

	            final File file = (File) request.getAttribute(ATTR_FILE);
	            return new FileSystemResource(file);
	        }
	    }
	
	
	@RequestMapping(value = "/films", method = RequestMethod.GET)
	public String goFilms (Model model, HttpServletRequest request) {
		String res = "home";
		Users user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null){
			Map<String, List<Film>> genres = filmService.findFilmByGenresMap();
			model.addAttribute("genres", genres);
			res = "films";
		}
		return res;
	}
	
	@RequestMapping(value = "/filmView/{id}", method = RequestMethod.GET)
	public String editar (Model modelo, @PathVariable("id") String id, HttpServletRequest request) {
		String res = "home";
		Users user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null){
			BigDecimal id2 = new BigDecimal(id);
			modelo.addAttribute("film", filmService.findFilmByID(id2));
			modelo.addAttribute("userfilm",this.userFNFS.findOne(new UserfilmnofinishPK(id2,user.getIdUser())));
			res = "filmView";
		}
			
		return res;
	}
	
	
	@RequestMapping(value = "/filmViewNoFinish/{id}/{time}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public void videoNoFinish(Model modelo, @PathVariable("id") String id,@PathVariable("time") String time,HttpServletRequest request) {
			System.out.println("ID " + id);
			System.out.println("Time " + time);

			Film  film = this.filmRepository.findOne(new BigDecimal(id));
			Users user = (Users)(request.getSession().getAttribute("loggedUser"));
			UserfilmnofinishPK userFNPK = new UserfilmnofinishPK(film.getIdFilm(),user.getIdUser());
			Userfilmnofinish userFN = this.userFNFS.findOne(userFNPK);
			Integer integ = Integer.parseInt(time);
			Double doubleTime = Double.parseDouble(time);
			Double mins = (doubleTime/60);
			integ = integ*1000;
			BigDecimal bg = new BigDecimal(integ);
			if(mins >= film.getDuration()) {
				Userfilmview userFilmView = new Userfilmview(user.getIdUser(),film.getIdFilm());
				this.userFV.save(userFilmView);
				if(userFN != null) {
					this.userFNFS.deleteFilm(userFNPK);
				}
			} else {
				if(userFN != null) {
					this.userFNFS.updateTime(bg,userFN.getUserfilmnofinishPK());
				} else {
					userFN = new Userfilmnofinish(userFNPK,bg);
					this.userFNFS.save(userFN);	
				}
				this.userFV.findOne(new UserfilmviewPK(user.getIdUser(),film.getIdFilm()));
				if(userFV != null) {
					this.userFV.delete(new UserfilmviewPK(user.getIdUser(),film.getIdFilm()));
				}
			}
	}
	
	@RequestMapping(value = "/filmViewUpdate/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String  updateTime(Model modelo, @PathVariable("id") String id,HttpServletRequest request) {
		String res = "00:00:00";
		BigDecimal id2 = new BigDecimal(id);
		Film  film = this.filmRepository.findOne(id2);
		Users user = (Users)(request.getSession().getAttribute("loggedUser"));
		UserfilmnofinishPK userFNPK = new UserfilmnofinishPK(film.getIdFilm(),user.getIdUser());
		Userfilmnofinish userFN = this.userFNFS.findOne(userFNPK);
		if(userFN != null){
			res = userFN.getTimeFormat();
		}
		return res;
	}
	
	

	@RequestMapping(value = "/search/{filter}", method = RequestMethod.GET)
	public String goSearch(Model model, @PathVariable("filter") String filter, HttpServletRequest request) {
		String res = "home";
		Users user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null){
			model.addAttribute("search", filter);
			
			model.addAttribute("filmsByTitle", this.filmService.getFilmsByTitle(filter.toUpperCase()));
			model.addAttribute("seriesByTitle",this.serieService.getSeriesByTitle(filter.toUpperCase()));
			model.addAttribute("filmsByDirector", this.filmService.getFilmsByDirector(filter.toUpperCase()));
			res = "search";
		}
		return res;
	}
}
