package es.inftel.lumiere.controller;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.UserRepository;
import es.inftel.lumiere.service.FilmService;
import es.inftel.lumiere.service.SerieService;
import es.inftel.lumiere.service.UserFilmNoFinishService;
import es.inftel.lumiere.service.UserService;
import es.inftel.lumiere.service.UserepisodenofinishService;
import es.inftel.lumiere.utils.Utils;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	UserepisodenofinishService userepisodenofinishService;
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	UserFilmNoFinishService userfilmnofinishService;
	
	@Autowired
	SerieService seriesService;
	
	private String messageError;


	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@ModelAttribute("userForm") UserForm userForm, Model model)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		String res = "login";
		if (userService.checkUserEmail(userForm.getEmail()) == null) {
			if(userForm.getPassword().equals(userForm.getPassword1())) {
				userService.save(userForm.getUser());
				model.addAttribute("userLogin", new Users());
			}else {
				model.addAttribute("error", "Passwords don't match");
				userForm = new UserForm();
				res = "register";
			}
		}else {
			model.addAttribute("error", "This email is already registered");
			userForm = new UserForm();
			res = "register";
		}
		return res;
	}


	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@ModelAttribute("userLogin") Users userLogin, Model model, HttpServletRequest request)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		String res = "login";
		Users user = userService.getUser(userLogin.getEmail(), userLogin.getPass());
		if (user != null) {
			request.getSession().setAttribute("loggedUser", user);  
			res = "redirect:/index";
		}else {
			model.addAttribute("error", "Incorrect email or password");
			userLogin.setEmail("");
			userLogin.setPass("");
		}
		return res;
	}
	
	@RequestMapping(value = "/myProfile", method = RequestMethod.GET)
	public String goToMyProfile(Model model, HttpServletRequest request) {
		String res = "home";
		Users user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null){
			UserForm userForm = new UserForm();
			model.addAttribute("user", user);
			model.addAttribute("userForm", userForm);
			if(this.messageError != "") {
				model.addAttribute("error",this.messageError);
			}
			this.messageError = "";
			res = "myProfile";
		}
		
		return res;
	}
	
	@RequestMapping(value = "/editPassword", method = RequestMethod.POST)
	public String editPassword (@ModelAttribute("userForm") UserForm u, Model model, HttpServletRequest request) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String res = "home";
		Users user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null){
			if(u.getPassword().equals(u.getPassword1())) {
				userService.updatePass(u.getPassword(), user.getIdUser());
			}else{
				this.messageError = "Passwords don't match";
			}
			res = "redirect:/myProfile";
		}
			
		u.setPassword("");
		u.setPassword1("");
		
		return res;
	}
	
	
	@RequestMapping(value = "/logOut", method = RequestMethod.GET)
	public String logOut(Model logOut, HttpServletRequest request) {
		request.getSession().removeAttribute("loggedUser");
		return "home";
	}

}