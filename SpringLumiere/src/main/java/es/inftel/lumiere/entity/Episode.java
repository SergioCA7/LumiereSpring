/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "EPISODE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Episode.findAll", query = "SELECT e FROM Episode e")
    , @NamedQuery(name = "Episode.findByTitle", query = "SELECT e FROM Episode e WHERE e.title = :title")
    , @NamedQuery(name = "Episode.findByIdEpisode", query = "SELECT e FROM Episode e WHERE e.episodePK.idEpisode = :idEpisode")
    , @NamedQuery(name = "Episode.findByIdSeason", query = "SELECT e FROM Episode e WHERE e.episodePK.idSeason = :idSeason")
    , @NamedQuery(name = "Episode.findByIdSeries", query = "SELECT e FROM Episode e WHERE e.episodePK.idSeries = :idSeries")
    , @NamedQuery(name = "Episode.findByDuration", query = "SELECT e FROM Episode e WHERE e.duration = :duration")
    , @NamedQuery(name = "Episode.findByLifeTime", query = "SELECT e FROM Episode e WHERE e.lifeTime = :lifeTime")})
public class Episode implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EpisodePK episodePK;
    @Size(max = 200)
    @Column(name = "TITLE")
    private String title;
    @Column(name = "DURATION")
    private BigDecimal duration;
    @Column(name = "LIFE_TIME")
    private BigDecimal lifeTime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "episode")
    private Collection<Userepisodenofinish> userepisodenofinishCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "episode")
    private Collection<Userepisodeview> userepisodeviewCollection;
    @JoinColumns({
        @JoinColumn(name = "ID_SEASON", referencedColumnName = "ID_SEASON", insertable = false, updatable = false)
        , @JoinColumn(name = "ID_SERIES", referencedColumnName = "ID_SERIES", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Season season;

    public Episode() {
    }

    public Episode(EpisodePK episodePK) {
        this.episodePK = episodePK;
    }

    public Episode(BigDecimal idEpisode, BigDecimal idSeason, BigDecimal idSeries) {
        this.episodePK = new EpisodePK(idEpisode, idSeason, idSeries);
    }

    public EpisodePK getEpisodePK() {
        return episodePK;
    }

    public void setEpisodePK(EpisodePK episodePK) {
        this.episodePK = episodePK;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public BigDecimal getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(BigDecimal lifeTime) {
        this.lifeTime = lifeTime;
    }

    @XmlTransient
    public Collection<Userepisodenofinish> getUserepisodenofinishCollection() {
        return userepisodenofinishCollection;
    }

    public void setUserepisodenofinishCollection(Collection<Userepisodenofinish> userepisodenofinishCollection) {
        this.userepisodenofinishCollection = userepisodenofinishCollection;
    }

    @XmlTransient
    public Collection<Userepisodeview> getUserepisodeviewCollection() {
        return userepisodeviewCollection;
    }

    public void setUserepisodeviewCollection(Collection<Userepisodeview> userepisodeviewCollection) {
        this.userepisodeviewCollection = userepisodeviewCollection;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (episodePK != null ? episodePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Episode)) {
            return false;
        }
        Episode other = (Episode) object;
        if ((this.episodePK == null && other.episodePK != null) || (this.episodePK != null && !this.episodePK.equals(other.episodePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Episode[ episodePK=" + episodePK + " ]";
    }
    
}
