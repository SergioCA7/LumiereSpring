/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "SECURITYQUESTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Securityquestion.findAll", query = "SELECT s FROM Securityquestion s")
    , @NamedQuery(name = "Securityquestion.findByIdQuestion", query = "SELECT s FROM Securityquestion s WHERE s.idQuestion = :idQuestion")
    , @NamedQuery(name = "Securityquestion.findByQuestion", query = "SELECT s FROM Securityquestion s WHERE s.question = :question")})
public class Securityquestion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_QUESTION")
    private BigDecimal idQuestion;
    @Size(max = 200)
    @Column(name = "QUESTION")
    private String question;
    @OneToMany(mappedBy = "idQuestion")
    private Collection<Users> usersCollection;

    public Securityquestion() {
    }

    public Securityquestion(BigDecimal idQuestion) {
        this.idQuestion = idQuestion;
    }

    public BigDecimal getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(BigDecimal idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @XmlTransient
    public Collection<Users> getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Collection<Users> usersCollection) {
        this.usersCollection = usersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idQuestion != null ? idQuestion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Securityquestion)) {
            return false;
        }
        Securityquestion other = (Securityquestion) object;
        if ((this.idQuestion == null && other.idQuestion != null) || (this.idQuestion != null && !this.idQuestion.equals(other.idQuestion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Securityquestion[ idQuestion=" + idQuestion + " ]";
    }
    
}
