/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "USEREPISODEVIEW")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userepisodeview.findAll", query = "SELECT u FROM Userepisodeview u")
    , @NamedQuery(name = "Userepisodeview.findByIdUser", query = "SELECT u FROM Userepisodeview u WHERE u.userepisodeviewPK.idUser = :idUser")
    , @NamedQuery(name = "Userepisodeview.findByIdSeason", query = "SELECT u FROM Userepisodeview u WHERE u.userepisodeviewPK.idSeason = :idSeason")
    , @NamedQuery(name = "Userepisodeview.findByIdEpisode", query = "SELECT u FROM Userepisodeview u WHERE u.userepisodeviewPK.idEpisode = :idEpisode")
    , @NamedQuery(name = "Userepisodeview.findByIdSeries", query = "SELECT u FROM Userepisodeview u WHERE u.userepisodeviewPK.idSeries = :idSeries")})
public class Userepisodeview implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserepisodeviewPK userepisodeviewPK;
    @Lob
    @Size(max = 0)
    @Column(name = "TIMESTAMP")
    private String timestamp;
    @JoinColumns({
        @JoinColumn(name = "ID_EPISODE", referencedColumnName = "ID_EPISODE", insertable = false, updatable = false)
        , @JoinColumn(name = "ID_SEASON", referencedColumnName = "ID_SEASON", insertable = false, updatable = false)
        , @JoinColumn(name = "ID_SERIES", referencedColumnName = "ID_SERIES", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Episode episode;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;

    public Userepisodeview() {
    }

    public Userepisodeview(UserepisodeviewPK userepisodeviewPK) {
        this.userepisodeviewPK = userepisodeviewPK;
    }

    public Userepisodeview(BigDecimal idUser, BigDecimal idSeason, BigDecimal idEpisode, BigDecimal idSeries) {
        this.userepisodeviewPK = new UserepisodeviewPK(idUser, idSeason, idEpisode, idSeries);
    }

    public UserepisodeviewPK getUserepisodeviewPK() {
        return userepisodeviewPK;
    }

    public void setUserepisodeviewPK(UserepisodeviewPK userepisodeviewPK) {
        this.userepisodeviewPK = userepisodeviewPK;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Episode getEpisode() {
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userepisodeviewPK != null ? userepisodeviewPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userepisodeview)) {
            return false;
        }
        Userepisodeview other = (Userepisodeview) object;
        if ((this.userepisodeviewPK == null && other.userepisodeviewPK != null) || (this.userepisodeviewPK != null && !this.userepisodeviewPK.equals(other.userepisodeviewPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Userepisodeview[ userepisodeviewPK=" + userepisodeviewPK + " ]";
    }
    
}
