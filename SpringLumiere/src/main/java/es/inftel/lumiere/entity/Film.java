/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "FILM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Film.findAll", query = "SELECT f FROM Film f")
    , @NamedQuery(name = "Film.findByIdFilm", query = "SELECT f FROM Film f WHERE f.idFilm = :idFilm")
    , @NamedQuery(name = "Film.findByTitle", query = "SELECT f FROM Film f WHERE f.title = :title")
    , @NamedQuery(name = "Film.findByYear", query = "SELECT f FROM Film f WHERE f.year = :year")
    , @NamedQuery(name = "Film.findByGenres", query = "SELECT f FROM Film f WHERE f.genres = :genres")
    , @NamedQuery(name = "Film.findByOrigen", query = "SELECT f FROM Film f WHERE f.origen = :origen")
    , @NamedQuery(name = "Film.findByRating", query = "SELECT f FROM Film f WHERE f.rating = :rating")
    , @NamedQuery(name = "Film.findByOurRating", query = "SELECT f FROM Film f WHERE f.ourRating = :ourRating")
    , @NamedQuery(name = "Film.findByDuration", query = "SELECT f FROM Film f WHERE f.duration = :duration")
    , @NamedQuery(name = "Film.findByDirector", query = "SELECT f FROM Film f WHERE f.director = :director")
    , @NamedQuery(name = "Film.findByImagePoster", query = "SELECT f FROM Film f WHERE f.imagePoster = :imagePoster")
    , @NamedQuery(name = "Film.findByImageNetflix", query = "SELECT f FROM Film f WHERE f.imageNetflix = :imageNetflix")
    , @NamedQuery(name = "Film.findByQuote", query = "SELECT f FROM Film f WHERE f.quote = :quote")
    , @NamedQuery(name = "Film.findByQuoteAuthor", query = "SELECT f FROM Film f WHERE f.quoteAuthor = :quoteAuthor")
    , @NamedQuery(name = "Film.findByLifeTime", query = "SELECT f FROM Film f WHERE f.lifeTime = :lifeTime")
    , @NamedQuery(name = "Film.findBySinopsis", query = "SELECT f FROM Film f WHERE f.sinopsis = :sinopsis")
    , @NamedQuery(name = "Film.findByYtLink", query = "SELECT f FROM Film f WHERE f.ytLink = :ytLink")})
public class Film implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_FILM")
    private BigDecimal idFilm;
    @Size(max = 200)
    @Column(name = "TITLE")
    private String title;
    @Column(name = "YEAR")
    private BigDecimal year;
    @Size(max = 500)
    @Column(name = "GENRES")
    private String genres;
    @Size(max = 200)
    @Column(name = "ORIGEN")
    private String origen;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "RATING")
    private Double rating;
    @Column(name = "OUR_RATING")
    private Double ourRating;
    @Column(name = "DURATION")
    private Double duration;
    @Size(max = 200)
    @Column(name = "DIRECTOR")
    private String director;
    @Size(max = 200)
    @Column(name = "IMAGE_POSTER")
    private String imagePoster;
    @Size(max = 200)
    @Column(name = "IMAGE_NETFLIX")
    private String imageNetflix;
    @Size(max = 250)
    @Column(name = "QUOTE")
    private String quote;
    @Size(max = 200)
    @Column(name = "QUOTE_AUTHOR")
    private String quoteAuthor;
    @Column(name = "LIFE_TIME")
    private BigDecimal lifeTime;
    @Size(max = 200)
    @Column(name = "SINOPSIS")
    private String sinopsis;
    @Size(max = 30)
    @Column(name = "YT_LINK")
    private String ytLink;
    @JoinTable(name = "USERFILMVIEW", joinColumns = {
        @JoinColumn(name = "ID_FILM", referencedColumnName = "ID_FILM")}, inverseJoinColumns = {
        @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER")})
    @ManyToMany
    private Collection<Users> usersCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "film")
    private Collection<Userfilmnofinish> userfilmnofinishCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "film")
    private Collection<Comments> commentsCollection;

    public Film() {
    }

    public Film(BigDecimal idFilm) {
        this.idFilm = idFilm;
    }

    public BigDecimal getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(BigDecimal idFilm) {
        this.idFilm = idFilm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getYear() {
        return year;
    }

    public void setYear(BigDecimal year) {
        this.year = year;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Double getOurRating() {
        return ourRating;
    }

    public void setOurRating(Double ourRating) {
        this.ourRating = ourRating;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getImagePoster() {
        return imagePoster;
    }

    public void setImagePoster(String imagePoster) {
        this.imagePoster = imagePoster;
    }

    public String getImageNetflix() {
        return imageNetflix;
    }

    public void setImageNetflix(String imageNetflix) {
        this.imageNetflix = imageNetflix;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getQuoteAuthor() {
        return quoteAuthor;
    }

    public void setQuoteAuthor(String quoteAuthor) {
        this.quoteAuthor = quoteAuthor;
    }

    public BigDecimal getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(BigDecimal lifeTime) {
        this.lifeTime = lifeTime;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getYtLink() {
        return ytLink.trim();
    }

    public void setYtLink(String ytLink) {
        this.ytLink = ytLink;
    }

    @XmlTransient
    public Collection<Users> getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Collection<Users> usersCollection) {
        this.usersCollection = usersCollection;
    }

    @XmlTransient
    public Collection<Userfilmnofinish> getUserfilmnofinishCollection() {
        return userfilmnofinishCollection;
    }

    public void setUserfilmnofinishCollection(Collection<Userfilmnofinish> userfilmnofinishCollection) {
        this.userfilmnofinishCollection = userfilmnofinishCollection;
    }

    @XmlTransient
    public Collection<Comments> getCommentsCollection() {
        return commentsCollection;
    }

    public void setCommentsCollection(Collection<Comments> commentsCollection) {
        this.commentsCollection = commentsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFilm != null ? idFilm.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Film)) {
            return false;
        }
        Film other = (Film) object;
        if ((this.idFilm == null && other.idFilm != null) || (this.idFilm != null && !this.idFilm.equals(other.idFilm))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Film[ idFilm=" + idFilm + " ]";
    }
    
    
    
}
