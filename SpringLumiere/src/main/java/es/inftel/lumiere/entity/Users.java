/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "USERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u")
    , @NamedQuery(name = "Users.findByIdUser", query = "SELECT u FROM Users u WHERE u.idUser = :idUser")
    , @NamedQuery(name = "Users.findByName", query = "SELECT u FROM Users u WHERE u.name = :name")
    , @NamedQuery(name = "Users.findByPass", query = "SELECT u FROM Users u WHERE u.pass = :pass")
    , @NamedQuery(name = "Users.findByAnswer", query = "SELECT u FROM Users u WHERE u.answer = :answer")
    , @NamedQuery(name = "Users.findBySurname", query = "SELECT u FROM Users u WHERE u.surname = :surname")
    , @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email")})
@SequenceGenerator(name="USERS_SEQ", sequenceName="USERS_SEQ", initialValue=1, allocationSize=10)
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USER")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USERS_SEQ")
    private BigDecimal idUser;
    @Size(max = 50)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "PASS")
    private String pass;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ANSWER")
    private String answer;
    @Size(max = 50)
    @Column(name = "SURNAME")
    private String surname;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL")
    private String email;
    @ManyToMany(mappedBy = "usersCollection")
    private Collection<Film> filmCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private Collection<Userepisodenofinish> userepisodenofinishCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private Collection<Userfilmnofinish> userfilmnofinishCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private Collection<Userepisodeview> userepisodeviewCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private Collection<Comments> commentsCollection;
    @JoinColumn(name = "ID_QUESTION", referencedColumnName = "ID_QUESTION")
    @ManyToOne
    private Securityquestion idQuestion;

    public Users() {
    }

    public Users(BigDecimal idUser) {
        this.idUser = idUser;
    }

    public Users(BigDecimal idUser, String pass, String answer, String email, String surname) {
        this.idUser = idUser;
        this.pass = pass;
        this.answer = answer;
        this.email = email;
        this.surname = surname;
    }
    public Users( String email, String name,String pass, String answer, String surname) { 
        this.email = email;
        this.name = name;
        this.pass = pass;
        this.answer = answer;
        this.surname = surname;
    }

    public BigDecimal getIdUser() {
        return idUser;
    }

    public void setIdUser(BigDecimal idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public Collection<Film> getFilmCollection() {
        return filmCollection;
    }

    public void setFilmCollection(Collection<Film> filmCollection) {
        this.filmCollection = filmCollection;
    }

    @XmlTransient
    public Collection<Userepisodenofinish> getUserepisodenofinishCollection() {
        return userepisodenofinishCollection;
    }

    public void setUserepisodenofinishCollection(Collection<Userepisodenofinish> userepisodenofinishCollection) {
        this.userepisodenofinishCollection = userepisodenofinishCollection;
    }

    @XmlTransient
    public Collection<Userfilmnofinish> getUserfilmnofinishCollection() {
        return userfilmnofinishCollection;
    }

    public void setUserfilmnofinishCollection(Collection<Userfilmnofinish> userfilmnofinishCollection) {
        this.userfilmnofinishCollection = userfilmnofinishCollection;
    }

    @XmlTransient
    public Collection<Userepisodeview> getUserepisodeviewCollection() {
        return userepisodeviewCollection;
    }

    public void setUserepisodeviewCollection(Collection<Userepisodeview> userepisodeviewCollection) {
        this.userepisodeviewCollection = userepisodeviewCollection;
    }

    @XmlTransient
    public Collection<Comments> getCommentsCollection() {
        return commentsCollection;
    }

    public void setCommentsCollection(Collection<Comments> commentsCollection) {
        this.commentsCollection = commentsCollection;
    }

    public Securityquestion getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Securityquestion idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUser != null ? idUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.idUser == null && other.idUser != null) || (this.idUser != null && !this.idUser.equals(other.idUser))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Users[ idUser=" + idUser + " ]";
    }
    
}
