/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "SERIES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Series.findAll", query = "SELECT s FROM Series s")
    , @NamedQuery(name = "Series.findByIdSeries", query = "SELECT s FROM Series s WHERE s.idSeries = :idSeries")
    , @NamedQuery(name = "Series.findByTitle", query = "SELECT s FROM Series s WHERE s.title = :title")
    , @NamedQuery(name = "Series.findBySinopsis", query = "SELECT s FROM Series s WHERE s.sinopsis = :sinopsis")
    , @NamedQuery(name = "Series.findByRating", query = "SELECT s FROM Series s WHERE s.rating = :rating")
    , @NamedQuery(name = "Series.findByOrigen", query = "SELECT s FROM Series s WHERE s.origen = :origen")
    , @NamedQuery(name = "Series.findByGenders", query = "SELECT s FROM Series s WHERE s.genders = :genders")
    , @NamedQuery(name = "Series.findByYear", query = "SELECT s FROM Series s WHERE s.year = :year")
    , @NamedQuery(name = "Series.findByOurRating", query = "SELECT s FROM Series s WHERE s.ourRating = :ourRating")
    , @NamedQuery(name = "Series.findByImageNetflix", query = "SELECT s FROM Series s WHERE s.imageNetflix = :imageNetflix")
    , @NamedQuery(name = "Series.findByQuote", query = "SELECT s FROM Series s WHERE s.quote = :quote")
    , @NamedQuery(name = "Series.findByQuoteAuthor", query = "SELECT s FROM Series s WHERE s.quoteAuthor = :quoteAuthor")
    , @NamedQuery(name = "Series.findByImagePoster", query = "SELECT s FROM Series s WHERE s.imagePoster = :imagePoster")
    , @NamedQuery(name = "Series.findByLifeTime", query = "SELECT s FROM Series s WHERE s.lifeTime = :lifeTime")
    , @NamedQuery(name = "Series.findByYtLink", query = "SELECT s FROM Series s WHERE s.ytLink = :ytLink")})
public class Series implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SERIES")
    private BigDecimal idSeries;
    @Size(max = 200)
    @Column(name = "TITLE")
    private String title;
    @Size(max = 400)
    @Column(name = "SINOPSIS")
    private String sinopsis;
    @Column(name = "RATING")
    private BigDecimal rating;
    @Size(max = 200)
    @Column(name = "ORIGEN")
    private String origen;
    @Size(max = 200)
    @Column(name = "GENDERS")
    private String genders;
    @Column(name = "YEAR")
    private BigDecimal year;
    @Column(name = "OUR_RATING")
    private BigDecimal ourRating;
    @Size(max = 200)
    @Column(name = "IMAGE_NETFLIX")
    private String imageNetflix;
    @Size(max = 250)
    @Column(name = "QUOTE")
    private String quote;
    @Size(max = 200)
    @Column(name = "QUOTE_AUTHOR")
    private String quoteAuthor;
    @Size(max = 200)
    @Column(name = "IMAGE_POSTER")
    private String imagePoster;
    @Column(name = "LIFE_TIME")
    private BigDecimal lifeTime;
    @Size(max = 30)
    @Column(name = "YT_LINK")
    private String ytLink;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "series")
    private Collection<Season> seasonCollection;

    public Series() {
    }

    public Series(BigDecimal idSeries) {
        this.idSeries = idSeries;
    }

    public BigDecimal getIdSeries() {
        return idSeries;
    }

    public void setIdSeries(BigDecimal idSeries) {
        this.idSeries = idSeries;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public BigDecimal getRating() {
        return rating;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getGenders() {
        return genders;
    }

    public void setGenders(String genders) {
        this.genders = genders;
    }

    public BigDecimal getYear() {
        return year;
    }

    public void setYear(BigDecimal year) {
        this.year = year;
    }

    public BigDecimal getOurRating() {
        return ourRating;
    }

    public void setOurRating(BigDecimal ourRating) {
        this.ourRating = ourRating;
    }

    public String getImageNetflix() {
        return imageNetflix;
    }

    public void setImageNetflix(String imageNetflix) {
        this.imageNetflix = imageNetflix;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getQuoteAuthor() {
        return quoteAuthor;
    }

    public void setQuoteAuthor(String quoteAuthor) {
        this.quoteAuthor = quoteAuthor;
    }

    public String getImagePoster() {
        return imagePoster;
    }

    public void setImagePoster(String imagePoster) {
        this.imagePoster = imagePoster;
    }

    public BigDecimal getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(BigDecimal lifeTime) {
        this.lifeTime = lifeTime;
    }

    public String getYtLink() {
        return ytLink;
    }

    public void setYtLink(String ytLink) {
        this.ytLink = ytLink;
    }

    @XmlTransient
    public Collection<Season> getSeasonCollection() {
        return seasonCollection;
    }

    public void setSeasonCollection(Collection<Season> seasonCollection) {
        this.seasonCollection = seasonCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSeries != null ? idSeries.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Series)) {
            return false;
        }
        Series other = (Series) object;
        if ((this.idSeries == null && other.idSeries != null) || (this.idSeries != null && !this.idSeries.equals(other.idSeries))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Series[ idSeries=" + idSeries + " ]";
    }
    
}
