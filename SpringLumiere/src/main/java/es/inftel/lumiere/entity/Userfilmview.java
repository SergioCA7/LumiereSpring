/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "USERFILMVIEW")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userfilmview.findAll", query = "SELECT u FROM Userfilmview u")
    , @NamedQuery(name = "Userfilmview.findByIdUser", query = "SELECT u FROM Userfilmview u WHERE u.userfilmviewPK.idUser = :idUser")
    , @NamedQuery(name = "Userfilmview.findByIdFilm", query = "SELECT u FROM Userfilmview u WHERE u.userfilmviewPK.idFilm = :idFilm")
    , @NamedQuery(name = "Userfilmview.findByTime", query = "SELECT u FROM Userfilmview u WHERE u.time = :time")})
public class Userfilmview implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserfilmviewPK userfilmviewPK;
    @Column(name = "TIME")
    private BigDecimal time;
    @JoinColumn(name = "ID_FILM", referencedColumnName = "ID_FILM", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Film film;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;

    public Userfilmview() {
    }

    public Userfilmview(UserfilmviewPK userfilmviewPK) {
        this.userfilmviewPK = userfilmviewPK;
    }

    public Userfilmview(BigDecimal idUser, BigDecimal idFilm) {
        this.userfilmviewPK = new UserfilmviewPK(idUser, idFilm);
    }

    public UserfilmviewPK getUserfilmviewPK() {
        return userfilmviewPK;
    }

    public void setUserfilmviewPK(UserfilmviewPK userfilmviewPK) {
        this.userfilmviewPK = userfilmviewPK;
    }

    public BigDecimal getTime() {
        return time;
    }

    public void setTime(BigDecimal time) {
        this.time = time;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userfilmviewPK != null ? userfilmviewPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userfilmview)) {
            return false;
        }
        Userfilmview other = (Userfilmview) object;
        if ((this.userfilmviewPK == null && other.userfilmviewPK != null) || (this.userfilmviewPK != null && !this.userfilmviewPK.equals(other.userfilmviewPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Userfilmview[ userfilmviewPK=" + userfilmviewPK + " ]";
    }
    
}
