/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "SEASON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Season.findAll", query = "SELECT s FROM Season s")
    , @NamedQuery(name = "Season.findByIdSeason", query = "SELECT s FROM Season s WHERE s.seasonPK.idSeason = :idSeason")
    , @NamedQuery(name = "Season.findByIdSeries", query = "SELECT s FROM Season s WHERE s.seasonPK.idSeries = :idSeries")
    , @NamedQuery(name = "Season.findByNumberCap", query = "SELECT s FROM Season s WHERE s.numberCap = :numberCap")
    , @NamedQuery(name = "Season.findByNumberSeason", query = "SELECT s FROM Season s WHERE s.numberSeason = :numberSeason")})
public class Season implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SeasonPK seasonPK;
    @Column(name = "NUMBER_CAP")
    private BigDecimal numberCap;
    @Column(name = "NUMBER_SEASON")
    private BigDecimal numberSeason;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "season")
    private Collection<Episode> episodeCollection;
    @JoinColumn(name = "ID_SERIES", referencedColumnName = "ID_SERIES", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Series series;

    public Season() {
    }

    public Season(SeasonPK seasonPK) {
        this.seasonPK = seasonPK;
    }

    public Season(BigDecimal idSeason, BigDecimal idSeries) {
        this.seasonPK = new SeasonPK(idSeason, idSeries);
    }

    public SeasonPK getSeasonPK() {
        return seasonPK;
    }

    public void setSeasonPK(SeasonPK seasonPK) {
        this.seasonPK = seasonPK;
    }

    public BigDecimal getNumberCap() {
        return numberCap;
    }

    public void setNumberCap(BigDecimal numberCap) {
        this.numberCap = numberCap;
    }
    
    public BigDecimal getNumberSeason() {
        return numberSeason;
    }

    public void setNumberSeason(BigDecimal numberSeason) {
        this.numberSeason = numberSeason;
    }

    @XmlTransient
    public Collection<Episode> getEpisodeCollection() {
        return episodeCollection;
    }

    public void setEpisodeCollection(Collection<Episode> episodeCollection) {
        this.episodeCollection = episodeCollection;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seasonPK != null ? seasonPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Season)) {
            return false;
        }
        Season other = (Season) object;
        if ((this.seasonPK == null && other.seasonPK != null) || (this.seasonPK != null && !this.seasonPK.equals(other.seasonPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Season[ seasonPK=" + seasonPK + " ]";
    }
    
}
