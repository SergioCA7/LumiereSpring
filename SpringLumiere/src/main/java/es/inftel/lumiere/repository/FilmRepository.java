package es.inftel.lumiere.repository;

import java.math.BigDecimal;
import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.inftel.lumiere.entity.Film;


public interface FilmRepository extends CrudRepository<Film, BigDecimal>{

	@Query("SELECT f FROM Film f WHERE f.genres = ?1")
	List <Film> findFilmByGenre(String genre);
	
	@Query("SELECT DISTINCT f.genres FROM Film f")
	List <String> findGenres();
	
	@Query("SELECT f FROM Film f WHERE f.idFilm = ?1")
	Film findFilmByID(BigDecimal id);
	
	@Query("SELECT f FROM Film f ORDER BY year DESC")
	List<Film> findTrendyFilms();
	
	@Query("SELECT f FROM Film f WHERE f.year =?1")
	List<Film> findLatestFilms(BigDecimal lastYear);
	
	@Query("SELECT f FROM Film f ORDER BY ourRating DESC ")
	List<Film> findTopRatedFilmsLumiereUsers();
	
	@Query("SELECT f FROM Film f ORDER BY rating DESC")
	List<Film> findTopRatedFilmsFilmaffinityUsers();
	
	@Query("SELECT f FROM Film f WHERE UPPER(title) LIKE  %?1%")
	List<Film> getFilmsByTitle(String title);
	
	@Query("SELECT f FROM Film f WHERE UPPER(director) LIKE %?1%")
	List<Film> getFilmsByDirector(String director);
	
}
