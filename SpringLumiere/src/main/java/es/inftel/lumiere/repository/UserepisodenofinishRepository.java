package es.inftel.lumiere.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.UserepisodenofinishPK;
import es.inftel.lumiere.entity.Users;


public interface UserepisodenofinishRepository extends CrudRepository <Userepisodenofinish,UserepisodenofinishPK>{

	@Query("SELECT enf FROM Userepisodenofinish enf WHERE enf.users =?1 ORDER BY timestamp DESC")
	List<Userepisodenofinish> findEpisodiesNotFinished (Users user);

}