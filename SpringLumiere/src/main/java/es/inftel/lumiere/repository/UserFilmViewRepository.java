package es.inftel.lumiere.repository;

import org.springframework.data.repository.CrudRepository;

import es.inftel.lumiere.entity.Userfilmview;
import es.inftel.lumiere.entity.UserfilmviewPK;
import es.inftel.lumiere.entity.Users;

public interface UserFilmViewRepository extends CrudRepository<Userfilmview, UserfilmviewPK>{

}
