package es.inftel.lumiere.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.math.BigDecimal;


import es.inftel.lumiere.entity.Series;

public interface SerieRepository extends CrudRepository<Series, BigDecimal> {
	
	@Query("SELECT s FROM Series s WHERE s.genders = ?1")
	List <Series> findSeriesByGenre(String genre);

	@Query("SELECT DISTINCT s.genders FROM Series s")
	List<String> findGenres();
	
	@Query("SELECT s FROM Series s WHERE year =?1")
	List<Series> findLatestSeries(BigDecimal lastYear);
	
	@Query("SELECT s FROM Series s ORDER BY ourRating DESC")
	List<Series> findTopRatedSeriesLumiereUsers();
	
	@Query("SELECT s FROM Series s ORDER BY rating DESC") 
	List<Series> findTopRatedSeriesFilmaffinityUsers();
	
	@Query("SELECT s FROM Series s WHERE UPPER(title) LIKE  %?1%")
	List<Series> getSeriesByTitle(@Param("title") String title);
	

}
