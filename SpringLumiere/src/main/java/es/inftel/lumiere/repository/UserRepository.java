package es.inftel.lumiere.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import es.inftel.lumiere.entity.UserfilmnofinishPK;
import es.inftel.lumiere.entity.Users;

public interface UserRepository extends CrudRepository<Users, BigDecimal> {
	
	@Query("SELECT u FROM Users u WHERE u.email = ?1 and u.pass = ?2")
	Users getUser(String email, String password);
	
	@Query("SELECT u FROM Users u WHERE u.email = ?1")
	 Users checkUserEmail(String email);
	
	@Transactional
	@Modifying
	@Query("UPDATE Users U SET U.pass =?1 WHERE U.idUser =?2")
	void updatePass(String pass, BigDecimal id_user);
}